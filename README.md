## Information

* Start server : `symfony server:start`

* Api url : /api

* Make entity : `php bin/console make:entity`

### start server pro

* Copy .env to .env.local and change values
* start docker `docker-compose up -d`
* Go into the container and run `./composer i`
