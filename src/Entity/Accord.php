<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

class Accord
{
    #[ORM\Column(type: 'string', length: 4)]
    private $mainKey;

    #[ORM\Column(type: 'array')]
    private $keys = [];

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    public function getMainKey(): ?string
    {
        return $this->mainKey;
    }

    public function setMainKey(string $mainKey): self
    {
        $this->mainKey = $mainKey;

        return $this;
    }

    public function getKeys(): ?array
    {
        return $this->keys;
    }

    public function setKeys(array $keys): self
    {
        $this->keys = $keys;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
