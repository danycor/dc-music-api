<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

class Gamme
{

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'string', length: 3)]
    private $mainKey;

    private $mode; // M -> maj, m -> min

    #[ORM\Column(type: 'array')]
    private $keys = [];

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMainKey(): ?string
    {
        return $this->mainKey;
    }

    public function setMainKey(string $mainKey): self
    {
        $this->mainKey = $mainKey;

        return $this;
    }

    public function getKeys(): ?array
    {
        return $this->keys;
    }

    public function setKeys(array $keys): self
    {
        $this->keys = $keys;

        return $this;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }


}
