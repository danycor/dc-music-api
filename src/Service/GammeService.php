<?php


namespace App\Service;


use App\Entity\Accord;
use App\Entity\Gamme;
use stdClass;

class GammeService
{
    private $majKeys = ["C" => 0, "D" => 2, "E" => 4, "F" => 5, "G" => 7, "A" => 9, "B" => 11];
    private $keys = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];
    private $gammes = [];

    public function __construct()
    {
        // maj
        foreach ($this->majKeys as $majKey => $keyValue) {
            $gamme = new Gamme();
            $gamme->setName($majKey."M");
            $gamme->setMode("M");
            $gamme->setMainKey($majKey);
            $gamme->setKeys($this->generateGamme($keyValue, [2, 2, 1, 2, 2, 2 ,1]));
            $this->gammes[] = $gamme;
        }

        // min
        foreach ($this->majKeys as $majKey => $keyValue) {
            $gamme = new Gamme();
            $gamme->setName($majKey."m");
            $gamme->setMode("m");
            $gamme->setMainKey($majKey);
            $gamme->setKeys($this->generateGamme($keyValue, [2, 1, 2, 2, 1, 2 ,2]));
            $this->gammes[] = $gamme;
        }
    }

    public function getAllGammes() {
        return $this->gammes;
    }

    public function getByName($name) {
        /** @var Gamme $gamme */
        foreach ($this->gammes as $gamme) {
            if ($gamme->getName() === $name) {
                return $gamme;
            }
        }
        return new stdClass();
    }

    public function getByKey($key) {
        $key = $this->getKeyLetter($key);
        return array_filter($this->gammes, function ($gamme) use ($key) {
           return in_array($key, $gamme->getKeys());
        });
    }

    public function getAccordsForGamme(Gamme $gamme) {
        $accords = [];
        $keys = $gamme->getKeys();

        $position = 0;
        foreach ($keys as $mainKey) {
            $accord = new Accord();
            $accord->setName($mainKey.$gamme->getMode());
            $accord->setMainKey($mainKey);
            $accord->setKeys([
                $keys[($position+1)%7], // 3
                $keys[($position+3)%7], // 5
                $keys[($position+6)%7], // 1
            ]);
            $position++;
            $accords[] = $accord;
        }
        return $accords;
    }

    public function getAccordWithKey(string $key1) {
        $accords = [];
        foreach ($this->gammes as $gamme) {
            $gamme_accords = $this->getAccordsForGamme($gamme);
            $accords = array_merge(
                $accords,
                array_filter($gamme_accords, function ($accord) use ($key1) {
                    return in_array($key1, $accord->getKeys());
                })
            );
        }
        return $accords;
    }

    public function getAccordWithKey2(string $key, string $key2) {
        $accords = [];
        foreach ($this->gammes as $gamme) {
            $gamme_accords = $this->getAccordsForGamme($gamme);
            $accords = array_merge(
                $accords,
                array_filter($gamme_accords, function ($accord) use ($key, $key2) {
                    return in_array($key, $accord->getKeys()) && in_array($key2, $accord->getKeys());
                })
            );
        }
        return $accords;
    }

    private function generateGamme($majKeyIndex, $interval) {
        $gamme = [];
        $position = $majKeyIndex;
        foreach ($interval as $nextPositionAdd) {
            $position += $nextPositionAdd;
            $position = $position % 12;
            $gamme[] = $this->keys[$position];
        }
        return $gamme;
    }

    private function getKeyLetter($key) {
        $value = $key;
        switch (strtolower($key)) {
            case "do" :   $value = "C"; break;
            case "do#" :  $value = "C#"; break;
            case "re" :   $value = "D"; break;
            case "re#" :  $value = "D#"; break;
            case "mi" :   $value = "E"; break;
            case "fa" :   $value = "F"; break;
            case "fa#" :  $value = "F#"; break;
            case "sol" :  $value = "G"; break;
            case "sol#" : $value = "G#"; break;
            case "la" :   $value = "A"; break;
            case "la#" :  $value = "A#"; break;
            case "si" :   $value = "B"; break;
        }
        return $value;
    }

}