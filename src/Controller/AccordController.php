<?php

namespace App\Controller;

use App\Service\GammeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccordController extends AbstractController
{
    #[Route('/accord/{gammeName}', name: 'app_accord')]
    public function index(string $gammeName, GammeService $gammeService): Response
    {
        $gamme = $gammeService->getByName($gammeName);
        return $this->json($gammeService->getAccordsForGamme($gamme));
    }

    #[Route('/accord-with-key/{key}', name: 'app_accord_with_key')]
    public function getAccordWithKey(string $key, GammeService $gammeService): Response
    {
        return $this->json($gammeService->getAccordWithKey($key));
    }

    #[Route('/accord-with-key/{key}/{key2}', name: 'app_accord_with_key2')]
    public function getAccordWithKey2(string $key, string $key2, GammeService $gammeService): Response
    {
        return $this->json($gammeService->getAccordWithKey2($key, $key2));
    }
}
