<?php

namespace App\Controller;

use App\Service\GammeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GammeController extends AbstractController
{
    #[Route('/gammes', name: 'app_gammes')]
    public function getGammes(GammeService $gammeService): Response
    {
        return $this->json($gammeService->getAllGammes());
    }

    #[Route('/gamme-by-name/{name}', name: 'app_gamme')]
    public function getGammeByName(string $name, GammeService $gammeService): Response
    {
        return $this->json($gammeService->getByName($name));
    }

    #[Route('/gammes-by-key/{key}', name: 'app_gamme_key')]
    public function getGammesByKey(string $key): Response
    {
        return $this->json($key);
    }
}
